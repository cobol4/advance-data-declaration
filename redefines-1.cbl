       IDENTIFICATION DIVISION.    
       PROGRAM-ID. REDEFINES-1.
       AUTHOR. NATCHA.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  INPUT-STRING   PIC X(8).
       01  WORK-AREA.
           02 FNUM  PIC 9(5) VALUE ZEROS.
           02 SNUM  PIC 99 VALUE ZEROS.
       01  WORK-NUM REDEFINES WORK-AREA PIC 99999V99.
       01  EDITED-NUM  PIC ZZ,ZZ9.99.

       PROCEDURE DIVISION.
       BEGIN.
           DISPLAY "Enter a decimal number - " WITH NO ADVANCING 
           ACCEPT INPUT-STRING 
           UNSTRING INPUT-STRING DELIMITED BY ".",
              INTO FNUM, SNUM
           MOVE WORK-NUM TO EDITED-NUM 
           DISPLAY "Decimal Number = " EDITED-NUM 
           ADD 10 TO WORK-NUM
           MOVE WORK-NUM TO EDITED-NUM
           DISPLAY "Decimal Number = " EDITED-NUM 
           . 